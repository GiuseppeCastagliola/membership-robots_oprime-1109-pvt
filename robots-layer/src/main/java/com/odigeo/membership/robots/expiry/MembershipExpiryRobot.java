package com.odigeo.membership.robots.expiry;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.expiration.MembershipExpirationService;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;
import org.apache.log4j.Logger;

import java.util.Map;

public class MembershipExpiryRobot extends AbstractRobot {
    private static final Logger LOGGER = Logger.getLogger(MembershipExpiryRobot.class);
    private final MembershipExpirationService membershipExpirationService;

    @Inject
    public MembershipExpiryRobot(MembershipExpirationService membershipExpirationService, @Assisted String robotId) {
        super(robotId);
        this.membershipExpirationService = membershipExpirationService;
    }

    @Override
    public RobotExecutionResult execute(Map<String, String> configuration) {
        membershipExpirationService.expireMemberships();
        LOGGER.info("Expiry Robot finished successfully");
        return RobotExecutionResult.OK;
    }
}
