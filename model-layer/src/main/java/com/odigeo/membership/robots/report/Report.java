package com.odigeo.membership.robots.report;

import java.util.Objects;
import java.util.StringJoiner;

public class Report {
    private static final String LINE_SEPARATOR = System.lineSeparator();
    private final StringJoiner errorJoiner;
    private final StringJoiner successJoiner;

    public Report() {
        errorJoiner = new StringJoiner(LINE_SEPARATOR);
        successJoiner = new StringJoiner(LINE_SEPARATOR);
    }

    public StringJoiner getErrorJoiner() {
        return errorJoiner;
    }

    public StringJoiner getSuccessJoiner() {
        return successJoiner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Report report = (Report) o;
        return Objects.equals(errorJoiner, report.errorJoiner) && Objects.equals(successJoiner, report.successJoiner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorJoiner, successJoiner);
    }
}
