package com.odigeo.membership.robots.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import java.util.StringJoiner;

public class SearchMembershipsDTO {
    private final LocalDate fromExpirationDate;
    private final LocalDate toExpirationDate;
    private final String status;
    private final String autoRenewal;
    private final BigDecimal minBalance;
    private final LocalDate fromActivationDate;
    private final boolean withMemberAccount;

    private SearchMembershipsDTO(SearchMembershipsDTOBuilder builder) {
        autoRenewal = builder.autoRenewal;
        withMemberAccount = builder.withMemberAccount;
        fromExpirationDate = builder.fromExpirationDate;
        toExpirationDate = builder.toExpirationDate;
        status = builder.status;
        minBalance = builder.minBalance;
        fromActivationDate = builder.fromActivationDate;
    }

    public static SearchMembershipsDTOBuilder builder() {
        return new SearchMembershipsDTOBuilder();
    }

    public LocalDate getFromExpirationDate() {
        return fromExpirationDate;
    }

    public LocalDate getToExpirationDate() {
        return toExpirationDate;
    }

    public String getStatus() {
        return status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public LocalDate getFromActivationDate() {
        return fromActivationDate;
    }

    public BigDecimal getMinBalance() {
        return minBalance;
    }

    public boolean isWithMemberAccount() {
        return withMemberAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchMembershipsDTO that = (SearchMembershipsDTO) o;
        return withMemberAccount == that.withMemberAccount
                && Objects.equals(fromExpirationDate, that.fromExpirationDate)
                && Objects.equals(toExpirationDate, that.toExpirationDate)
                && Objects.equals(status, that.status)
                && Objects.equals(minBalance, that.minBalance)
                && Objects.equals(fromActivationDate, that.fromActivationDate)
                && Objects.equals(autoRenewal, that.autoRenewal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromExpirationDate, toExpirationDate, status, autoRenewal, minBalance, fromActivationDate, withMemberAccount);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SearchMembershipsDTO.class.getSimpleName() + "[", "]")
                .add("fromExpirationDate=" + fromExpirationDate)
                .add("toExpirationDate=" + toExpirationDate)
                .add("status='" + status + "'")
                .add("autoRenewal='" + autoRenewal + "'")
                .add("minBalance='" + minBalance + "'")
                .add("fromActivationDate=" + fromActivationDate)
                .toString();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class SearchMembershipsDTOBuilder {
        private BigDecimal minBalance;
        private LocalDate fromExpirationDate;
        private LocalDate toExpirationDate;
        private String status;
        private String autoRenewal;
        private LocalDate fromActivationDate;
        private boolean withMemberAccount;

        public SearchMembershipsDTOBuilder fromExpirationDate(LocalDate fromExpirationDate) {
            this.fromExpirationDate = fromExpirationDate;
            return this;
        }

        public SearchMembershipsDTOBuilder toExpirationDate(LocalDate toExpirationDate) {
            this.toExpirationDate = toExpirationDate;
            return this;
        }

        public SearchMembershipsDTOBuilder status(String status) {
            this.status = status;
            return this;
        }

        public SearchMembershipsDTOBuilder autoRenewal(String autoRenewal) {
            this.autoRenewal = autoRenewal;
            return this;
        }

        public SearchMembershipsDTOBuilder minBalance(BigDecimal minBalance) {
            this.minBalance = minBalance;
            return this;
        }

        public SearchMembershipsDTOBuilder withMemberAccount(boolean withMemberAccount) {
            this.withMemberAccount = withMemberAccount;
            return this;
        }

        public SearchMembershipsDTOBuilder fromActivationDate(LocalDate fromActivationDate) {
            this.fromActivationDate = fromActivationDate;
            return this;
        }

        public SearchMembershipsDTO build() {
            return new SearchMembershipsDTO(this);
        }

    }
}
