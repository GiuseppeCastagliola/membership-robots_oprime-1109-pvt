package com.odigeo.membership.robots.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@SuppressWarnings("PMD.GodClass")
public class MembershipDTO {
    private final long id;
    private final String website;
    private final String status;
    private final String autoRenewal;
    private final long memberAccountId;
    private final LocalDateTime expirationDate;
    private final LocalDateTime activationDate;
    private final LocalDateTime timestamp;
    private final BigDecimal userCreditCardId;
    private final String membershipType;
    private final BigDecimal balance;
    private final Integer monthsDuration;
    private final String productStatus;
    private final BigDecimal totalPrice;
    private final String currencyCode;
    private final String sourceType;
    private final String recurringId;
    private final String name;
    private final String lastName;
    private final Long userId;

    private MembershipDTO(Builder builder) {
        id = builder.id;
        userCreditCardId = builder.userCreditCardId;
        status = builder.status;
        totalPrice = builder.totalPrice;
        productStatus = builder.productStatus;
        recurringId = builder.recurringId;
        activationDate = builder.activationDate;
        expirationDate = builder.expirationDate;
        timestamp = builder.timestamp;
        monthsDuration = builder.monthsDuration;
        sourceType = builder.sourceType;
        balance = builder.balance;
        autoRenewal = builder.autoRenewal;
        memberAccountId = builder.memberAccountId;
        website = builder.website;
        membershipType = builder.membershipType;
        currencyCode = builder.currencyCode;
        name = builder.name;
        lastName = builder.lastName;
        userId = builder.userId;
    }

    public long getId() {
        return id;
    }

    public String getWebsite() {
        return website;
    }

    public String getStatus() {
        return status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public BigDecimal getUserCreditCardId() {
        return userCreditCardId;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Integer getMonthsDuration() {
        return monthsDuration;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builderFromDto(MembershipDTO membershipDTO) {
        return new Builder()
                .id(membershipDTO.id)
                .userCreditCardId(membershipDTO.userCreditCardId)
                .status(membershipDTO.status)
                .totalPrice(membershipDTO.totalPrice)
                .productStatus(membershipDTO.productStatus)
                .recurringId(membershipDTO.recurringId)
                .activationDate(membershipDTO.activationDate)
                .expirationDate(membershipDTO.expirationDate)
                .timestamp(membershipDTO.timestamp)
                .monthsDuration(membershipDTO.monthsDuration)
                .sourceType(membershipDTO.sourceType)
                .balance(membershipDTO.balance)
                .autoRenewal(membershipDTO.autoRenewal)
                .memberAccountId(membershipDTO.memberAccountId)
                .website(membershipDTO.website)
                .membershipType(membershipDTO.membershipType)
                .currencyCode(membershipDTO.currencyCode)
                .name(membershipDTO.name)
                .lastName(membershipDTO.lastName)
                .userId(membershipDTO.userId)
                .totalPrice(membershipDTO.totalPrice);
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getUserId() {
        return userId;
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private long id;
        private String website;
        private String status;
        private String autoRenewal;
        private long memberAccountId;
        private LocalDateTime expirationDate;
        private LocalDateTime activationDate;
        private LocalDateTime timestamp;
        private BigDecimal userCreditCardId;
        private String membershipType;
        private BigDecimal balance;
        private Integer monthsDuration;
        private String productStatus;
        private BigDecimal totalPrice;
        private String currencyCode;
        private String sourceType;
        private String recurringId;
        private String name;
        private String lastName;
        private Long userId;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder autoRenewal(String autoRenewal) {
            this.autoRenewal = autoRenewal;
            return this;
        }

        public Builder memberAccountId(long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        public Builder expirationDate(LocalDateTime expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder activationDate(LocalDateTime activationDate) {
            this.activationDate = activationDate;
            return this;
        }

        public Builder userCreditCardId(BigDecimal userCreditCardId) {
            this.userCreditCardId = userCreditCardId;
            return this;
        }

        public Builder membershipType(String membershipType) {
            this.membershipType = membershipType;
            return this;
        }

        public Builder balance(BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        public Builder monthsDuration(Integer monthsDuration) {
            this.monthsDuration = monthsDuration;
            return this;
        }

        public Builder productStatus(String productStatus) {
            this.productStatus = productStatus;
            return this;
        }

        public Builder totalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder sourceType(String sourceType) {
            this.sourceType = sourceType;
            return this;
        }

        public Builder recurringId(String recurringId) {
            this.recurringId = recurringId;
            return this;
        }

        public Builder timestamp(LocalDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public MembershipDTO build() {
            return new MembershipDTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipDTO that = (MembershipDTO) o;
        return id == that.id
                && memberAccountId == that.memberAccountId
                && Objects.equals(website, that.website)
                && Objects.equals(status, that.status)
                && Objects.equals(autoRenewal, that.autoRenewal)
                && Objects.equals(expirationDate, that.expirationDate)
                && Objects.equals(activationDate, that.activationDate)
                && Objects.equals(timestamp, that.timestamp)
                && Objects.equals(userCreditCardId, that.userCreditCardId)
                && Objects.equals(membershipType, that.membershipType)
                && Objects.equals(balance, that.balance)
                && Objects.equals(monthsDuration, that.monthsDuration)
                && Objects.equals(productStatus, that.productStatus)
                && Objects.equals(totalPrice, that.totalPrice)
                && Objects.equals(currencyCode, that.currencyCode)
                && Objects.equals(sourceType, that.sourceType)
                && Objects.equals(recurringId, that.recurringId)
                && Objects.equals(name, that.name)
                && Objects.equals(lastName, that.lastName)
                && Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, website, status, autoRenewal, memberAccountId, expirationDate, activationDate, timestamp,
                userCreditCardId, membershipType, balance, monthsDuration, productStatus, totalPrice, currencyCode,
                sourceType, recurringId, name, lastName, userId);
    }
}
