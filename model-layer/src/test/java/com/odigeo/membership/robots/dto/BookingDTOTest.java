package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.Collections;

public class BookingDTOTest extends BeanTest<BookingDTO> {

    private static final String CURRENCY_CODE = "EUR";
    private static final long BOOKING_ID = 123L;
    private static final long MEMBERSHIP_ID = 321L;
    private static final long USER_ID = 543L;

    @Override
    protected BookingDTO getBean() {
        return BookingDTO.builder()
                .currencyCode(CURRENCY_CODE)
                .feeContainers(Collections.singletonList(FeeContainerDTO.builder().build()))
                .id(BOOKING_ID)
                .membershipId(MEMBERSHIP_ID)
                .userId(USER_ID)
                .build();
    }

    @Test
    public void bookingDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(BookingDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
