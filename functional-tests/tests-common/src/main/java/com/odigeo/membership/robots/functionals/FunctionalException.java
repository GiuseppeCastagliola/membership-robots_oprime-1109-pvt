package com.odigeo.membership.robots.functionals;

public class FunctionalException extends RuntimeException {
    public FunctionalException(Throwable cause) {
        super(cause);
    }
}
