package com.odigeo.membership.robots.expiration;

public interface MembershipExpirationService {
    void expireMemberships();
}
