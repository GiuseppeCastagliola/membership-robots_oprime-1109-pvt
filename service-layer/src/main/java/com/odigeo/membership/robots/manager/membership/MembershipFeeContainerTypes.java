package com.odigeo.membership.robots.manager.membership;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes.AE10;
import static com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes.AE11;

public enum MembershipFeeContainerTypes {
    MEMBERSHIP_SUBSCRIPTION(AE11),
    MEMBERSHIP_RENEWAL(AE10);


    private final MembershipFeeSubCodes subCode;

    MembershipFeeContainerTypes(MembershipFeeSubCodes subcode) {
        this.subCode = subcode;
    }

    public static List<String> getAllSubCodes() {
        return Arrays.stream(values())
                .map(value -> value.subCode.name())
                .collect(Collectors.toList());
    }

    public MembershipFeeSubCodes getSubCode() {
        return subCode;
    }
}
