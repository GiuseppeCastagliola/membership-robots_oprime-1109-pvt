package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MembershipRequestMapper {

    @Mapping(target = "membershipId", source = "id")
    @Mapping(target = "operation", constant = "EXPIRE_MEMBERSHIP")
    @Mapping(target = "membershipRenewal", ignore = true)
    @Mapping(target = "membershipStatus", ignore = true)
    @Mapping(target = "userCreditCardId", ignore = true)
    UpdateMembershipRequest dtoToExpireRequest(MembershipDTO membershipDTO);

    @Mapping(target = "withWebsite", source = "membershipDTO.website")
    @Mapping(target = "withSubscriptionPrice", source = "membershipOfferDTO.price")
    @Mapping(target = "withRecurringId", source = "membershipDTO.recurringId")
    @Mapping(target = "withSourceType", source = "membershipDTO.sourceType")
    @Mapping(target = "withMembershipType", source = "membershipDTO.membershipType")
    @Mapping(target = "withExpirationDate", source = "newExpirationDate")
    @Mapping(target = "withMonthsToRenewal", constant = "12")
    @Mapping(target = "withMemberAccountId", source = "membershipDTO.memberAccountId")
    @Mapping(target = "withCurrencyCode", source = "membershipOfferDTO.currencyCode")
    CreatePendingToCollectRequest.Builder dtoToCreatePendingToCollectRequestBuilder(MembershipDTO membershipDTO, MembershipOfferDTO membershipOfferDTO, String newExpirationDate);

    @Mapping(target = "membershipId", source = "id")
    @Mapping(target = "operation", constant = "CONSUME_MEMBERSHIP_REMNANT_BALANCE")
    @Mapping(target = "membershipRenewal", ignore = true)
    @Mapping(target = "membershipStatus", ignore = true)
    @Mapping(target = "userCreditCardId", ignore = true)
    UpdateMembershipRequest dtoToResetBalanceRequest(MembershipDTO membershipDTO);
}
