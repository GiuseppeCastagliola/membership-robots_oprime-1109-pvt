package com.odigeo.membership.robots.configuration;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class ExpirationPeriod {
    private long daysInThePast;
    private String startingFrom;

    public long getDaysInThePast() {
        return daysInThePast;
    }

    public void setDaysInThePast(long daysInThePast) {
        this.daysInThePast = daysInThePast;
    }

    public String getStartingFrom() {
        return startingFrom;
    }

    public void setStartingFrom(String startingFrom) {
        this.startingFrom = startingFrom;
    }
}
