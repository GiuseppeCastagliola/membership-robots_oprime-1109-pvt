package com.odigeo.membership.robots.mapper.response;

import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface MembershipResponseMapper {

    @Mapping(target = "status", constant = "EXPIRED")
    MembershipDTO dtoToExpiredDto(MembershipDTO membershipDTO);

    @Mapping(target = "balance", constant = "0")
    MembershipDTO dtoToConsumedBalanceDto(MembershipDTO membershipDTO);

    @Mapping(target = "userId", source = "memberAccount.userId")
    @Mapping(target = "lastName", source = "memberAccount.lastNames")
    @Mapping(target = "name", source = "memberAccount.name")
    MembershipDTO membershipResponseToDto(MembershipResponse membershipResponse);

    List<MembershipDTO> membershipResponsesToDtos(List<MembershipResponse> membershipResponses);
}
