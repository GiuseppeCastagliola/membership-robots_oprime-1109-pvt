package com.odigeo.membership.robots.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.membership.robots.expiration.MembershipExpirationService;
import com.odigeo.membership.robots.expiration.MembershipExpirationServiceBean;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.manager.ExternalModuleManagerBean;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManagerBean;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManagerBean;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManagerBean;
import com.odigeo.membership.robots.manager.offer.MembershipOfferModuleManager;
import com.odigeo.membership.robots.manager.offer.MembershipOfferModuleManagerBean;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.mapper.request.MembershipOfferRequestMapper;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.request.MembershipSearchRequestMapper;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import com.odigeo.membership.robots.mapper.response.MembershipOfferResponseMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import com.odigeo.membership.robots.report.Reporter;
import com.odigeo.membership.robots.report.ReporterImpl;
import org.mapstruct.factory.Mappers;

public class MembershipRobotsModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(MembershipOfferModuleManager.class).to(MembershipOfferModuleManagerBean.class);
        bind(MembershipModuleManager.class).to(MembershipModuleManagerBean.class);
        bind(MembershipSearchModuleManager.class).to(MembershipSearchModuleManagerBean.class);
        bind(BookingApiManager.class).to(BookingApiManagerBean.class);
        bind(ExternalModuleManager.class).to(ExternalModuleManagerBean.class);
        bind(MembershipExpirationService.class).to(MembershipExpirationServiceBean.class);
        bind(Reporter.class).to(ReporterImpl.class);
        bind(MembershipRequestMapper.class).toInstance(Mappers.getMapper(MembershipRequestMapper.class));
        bind(MembershipResponseMapper.class).toInstance(Mappers.getMapper(MembershipResponseMapper.class));
        bind(MembershipOfferRequestMapper.class).toInstance(Mappers.getMapper(MembershipOfferRequestMapper.class));
        bind(MembershipOfferResponseMapper.class).toInstance(Mappers.getMapper(MembershipOfferResponseMapper.class));
        bind(MembershipSearchRequestMapper.class).toInstance(Mappers.getMapper(MembershipSearchRequestMapper.class));
        bind(BookingRequestMapper.class).toInstance(Mappers.getMapper(BookingRequestMapper.class));
        bind(BookingResponseMapper.class).toInstance(Mappers.getMapper(BookingResponseMapper.class));
    }
}
