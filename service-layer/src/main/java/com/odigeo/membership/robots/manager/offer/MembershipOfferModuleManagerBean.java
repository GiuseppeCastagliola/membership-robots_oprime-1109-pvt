package com.odigeo.membership.robots.manager.offer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.offer.api.exception.InvalidParametersException;
import com.odigeo.membership.offer.api.exception.MembershipOfferServiceException;
import com.odigeo.membership.offer.api.request.SearchCriteriaRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import com.odigeo.membership.robots.apicall.ApiCall.Result;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.mapper.request.MembershipOfferRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipOfferResponseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;

@Singleton
public class MembershipOfferModuleManagerBean implements MembershipOfferModuleManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipOfferModuleManagerBean.class);
    private final MembershipOfferService membershipOfferService;
    private final MembershipOfferRequestMapper membershipOfferRequestMapper;
    private final MembershipOfferResponseMapper membershipOfferResponseMapper;

    @Inject
    public MembershipOfferModuleManagerBean(MembershipOfferService membershipOfferService, MembershipOfferRequestMapper membershipOfferRequestMapper, MembershipOfferResponseMapper membershipOfferResponseMapper) {
        this.membershipOfferService = membershipOfferService;
        this.membershipOfferRequestMapper = membershipOfferRequestMapper;
        this.membershipOfferResponseMapper = membershipOfferResponseMapper;
    }

    @Override
    public ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getOffer(MembershipDTO membershipDTO) {
        ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO> getOfferWrapperBuilder = new ApiCallWrapperBuilder<>(Endpoint.GET_OFFER);
        SearchCriteriaRequest searchCriteriaRequest = membershipOfferRequestMapper.dtoToSearchCriteriaRequest(membershipDTO);
        final MembershipSubscriptionOffer membershipSubscriptionOffer;
        try {
            membershipSubscriptionOffer = membershipOfferService.getMembershipSubscriptionOffer(searchCriteriaRequest);
        } catch (InvalidParametersException | MembershipOfferServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            return getOfferWrapperBuilder.result(Result.FAIL).exception(e).message(e.getMessage()).build();
        }
        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getOfferWrapper = getOfferWrapperBuilder.result(Result.SUCCESS)
                .response(membershipOfferResponseMapper.subscriptionOfferToMembershipOfferDto(membershipSubscriptionOffer))
                .build();
        getOfferWrapper.logResultAndMessageWithPrefix(LOGGER, getOfferWrapper.getEndpoint() + " for website : " + membershipDTO.getWebsite());
        return getOfferWrapper;
    }
}
