package com.odigeo.membership.robots.manager.bookingapi;

import com.odigeo.bookingapi.v9.requests.FeeRequest;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;

import java.math.BigDecimal;

public class FeeRequestBuilder {
    private String subCode;
    private BigDecimal amount;
    private String currency;

    public FeeRequestBuilder withSubCode(MembershipFeeSubCodes subCode) {
        this.subCode = subCode.name();
        return this;
    }

    public FeeRequestBuilder withAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public FeeRequestBuilder withCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public FeeRequest build() {
        FeeRequest feeRequest = new FeeRequest();
        feeRequest.setAmount(amount);
        feeRequest.setCurrency(currency);
        feeRequest.setSubCode(subCode);
        return feeRequest;
    }
}
