package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.offer.api.request.SearchCriteriaRequest;
import com.odigeo.membership.offer.api.request.TestTokenSet;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Mapper(imports = {StringUtils.class})
public interface MembershipOfferRequestMapper {

    @Mapping(target = "searchCriteria.membershipId", source = "id")
    @Mapping(target = "searchCriteria.websiteCode", source = "website")
    @Mapping(target = "searchCriteria.hasAccommodation", constant = "false")
    @Mapping(target = "searchCriteria.hasItinerary", constant = "true")
    @Mapping(target = "searchCriteria.dynpackSearch", constant = "false")
    @Mapping(target = "searchCriteria.anInterface", constant = "OFFLINE")
    @Mapping(target = "searchCriteria.firstItineraryDate", expression = "java(today())")
    @Mapping(target = "searchCriteria.lastItineraryDate", expression = "java(tomorrow())")
    @Mapping(target = "searchCriteria.marketingPortal", expression = "java(StringUtils.EMPTY)")
    @Mapping(target = "searchCriteria.qaMode", constant = "false")
    @Mapping(target = "searchCriteria.visitId", constant = "1L")
    @Mapping(target = "searchCriteria.testTokenSet", expression = "java(avoidSixMonthsPriceAB())")
    SearchCriteriaRequest dtoToSearchCriteriaRequest(MembershipDTO membershipDTO);

    default TestTokenSet avoidSixMonthsPriceAB() {
        Map<String, Integer> dimensionPartitionNumberMap = new HashMap<>();
        dimensionPartitionNumberMap.put("X20-4", 1);
        return new TestTokenSet(dimensionPartitionNumberMap);
    }

    default String today() {
        return DateTimeFormatter.ISO_DATE.format(LocalDate.now());
    }

    default String tomorrow() {
        return DateTimeFormatter.ISO_DATE.format(LocalDate.now().plusDays(1L));
    }
}
