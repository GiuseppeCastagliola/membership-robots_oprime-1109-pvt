package com.odigeo.membership.robots.manager.membership.search;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;

import java.util.List;

public interface MembershipSearchModuleManager {
    ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships(SearchMembershipsDTO searchMembershipsDTO);
}
