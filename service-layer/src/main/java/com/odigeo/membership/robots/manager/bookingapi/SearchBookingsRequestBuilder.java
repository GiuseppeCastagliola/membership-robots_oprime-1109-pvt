package com.odigeo.membership.robots.manager.bookingapi;

import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;

public class SearchBookingsRequestBuilder {

    private Long membershipId;
    private String bookingSubscriptionPrime;

    public SearchBookingsRequest build() {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequest();
        searchBookingsRequest.setMembershipId(membershipId);
        searchBookingsRequest.setIsBookingSubscriptionPrime(bookingSubscriptionPrime);
        return searchBookingsRequest;
    }
    public SearchBookingsRequestBuilder membershipId(Long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public SearchBookingsRequestBuilder bookingSubscriptionPrime(Boolean bookingSubscriptionPrime) {
        this.bookingSubscriptionPrime = bookingSubscriptionPrime.toString();
        return this;
    }
}
