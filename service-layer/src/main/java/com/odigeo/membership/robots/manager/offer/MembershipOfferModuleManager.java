package com.odigeo.membership.robots.manager.offer;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;

public interface MembershipOfferModuleManager {
    ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getOffer(MembershipDTO membershipDTO);
}
