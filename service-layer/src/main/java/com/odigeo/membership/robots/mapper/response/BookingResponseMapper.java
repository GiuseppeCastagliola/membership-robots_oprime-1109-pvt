package com.odigeo.membership.robots.mapper.response;

import com.google.common.base.Preconditions;
import com.odigeo.bookingapi.v9.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.robots.dto.BookingDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@SuppressWarnings("PMD.AvoidDuplicateLiterals")
@Mapper
public interface BookingResponseMapper {

    @Mapping(target = "id", source = "bookingBasicInfo.id")
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "feeContainers", source = "allFeeContainers")
    @Mapping(target = "currencyCode", source = "currencyCode")
    @Mapping(target = "membershipId", ignore = true)
    BookingDTO bookingDetailResponseToDto(BookingDetail bookingDetail);

    @Mapping(target = "id", source = "bookingBasicInfo.id")
    @Mapping(target = "membershipId", source = "membershipId")
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "feeContainers", ignore = true)
    @Mapping(target = "currencyCode", ignore = true)
    BookingDTO bookingSummaryResponseToDto(BookingSummary bookingSummary);

    @Mapping(target = "id", expression = "java(verifyBookingId(bookingSummary, bookingDetail))")
    @Mapping(target = "membershipId", source = "bookingSummary.membershipId")
    @Mapping(target = "userId", source = "bookingDetail.userId")
    @Mapping(target = "feeContainers", source = "bookingDetail.feeContainers")
    @Mapping(target = "currencyCode", source = "bookingDetail.currencyCode")
    BookingDTO bookingResponsesToDto(BookingDTO bookingSummary, BookingDTO bookingDetail);

    List<BookingDTO> bookingSummariesResponseToDto(List<BookingSummary> bookingSummaries);

    default long verifyBookingId(BookingDTO bookingSummary, BookingDTO bookingDetail) {
        Preconditions.checkState(bookingSummary.getId() == bookingDetail.getId(), "bookingSummaryId: %s != bookingDetailId: %s", bookingSummary.getId(), bookingDetail.getId());
        return bookingSummary.getId();
    }
}
