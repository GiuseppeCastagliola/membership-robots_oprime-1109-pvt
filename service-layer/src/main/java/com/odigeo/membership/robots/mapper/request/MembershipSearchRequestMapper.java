package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface MembershipSearchRequestMapper {

    @Mapping(target = "toCreationDate", ignore = true)
    @Mapping(target = "toActivationDate", ignore = true)
    @Mapping(target = "productStatus", ignore = true)
    @Mapping(target = "monthsDuration", ignore = true)
    @Mapping(target = "website", ignore = true)
    @Mapping(target = "totalPrice", ignore = true)
    @Mapping(target = "sourceType", ignore = true)
    @Mapping(target = "membershipType", ignore = true)
    @Mapping(target = "maxBalance", ignore = true)
    @Mapping(target = "withStatusActions", ignore = true)
    @Mapping(target = "memberAccountSearchRequest", ignore = true)
    @Mapping(target = "fromCreationDate", ignore = true)
    @Mapping(target = "currencyCode", ignore = true)
    MembershipSearchRequest.Builder searchDtoToSearchRequestBuilder(SearchMembershipsDTO searchDTO);
}
