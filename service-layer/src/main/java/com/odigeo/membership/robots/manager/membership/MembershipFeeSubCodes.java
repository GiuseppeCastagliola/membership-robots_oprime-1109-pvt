package com.odigeo.membership.robots.manager.membership;

public enum MembershipFeeSubCodes {
    AE10, AE11, AC12
}
