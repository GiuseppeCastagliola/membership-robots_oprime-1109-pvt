package com.odigeo.membership.robots.report;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;

public interface Reporter {
    void generateReportLog(Report report);

    void feedReport(ApiCallWrapper<MembershipDTO, MembershipDTO> callWrapper, Report report);
}
