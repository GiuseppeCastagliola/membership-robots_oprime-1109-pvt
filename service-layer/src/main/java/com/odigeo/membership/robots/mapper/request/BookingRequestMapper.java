package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.manager.bookingapi.SearchBookingsRequestBuilder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface BookingRequestMapper {

    @Mapping(target = "membershipId", source = "id")
    @Mapping(target = "bookingSubscriptionPrime", constant = "true")
    SearchBookingsRequestBuilder dtoToPrimeSubscriptionBookingSummaryRequest(MembershipDTO membershipDTO);
}
