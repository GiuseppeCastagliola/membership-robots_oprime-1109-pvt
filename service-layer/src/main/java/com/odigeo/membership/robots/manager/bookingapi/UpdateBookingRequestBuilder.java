package com.odigeo.membership.robots.manager.bookingapi;

import com.odigeo.bookingapi.v9.requests.FeeRequest;
import com.odigeo.bookingapi.v9.requests.UpdateBookingRequest;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class UpdateBookingRequestBuilder {
    private List<FeeRequest> feeRequests;

    public UpdateBookingRequest build() {
        UpdateBookingRequest updateBookingRequest = new UpdateBookingRequest();
        updateBookingRequest.setBookingFees(feeRequests);
        return updateBookingRequest;
    }

    public UpdateBookingRequestBuilder withFeeRequests(List<FeeRequest> feeRequests) {
        this.feeRequests = unmodifiableList(feeRequests);
        return this;
    }
}

