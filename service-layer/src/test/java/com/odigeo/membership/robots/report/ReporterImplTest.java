package com.odigeo.membership.robots.report;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mock;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.StringJoiner;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ReporterImplTest {
    private static final String FAIL_MSG = "failed";
    private static final StringJoiner STRING_JOINER = new StringJoiner(StringUtils.EMPTY);
    private static final String SUCCESS_MSG = "success";
    @Spy
    private ReporterImpl reportService;
    @Mock
    private Report report;
    @Mock
    private MembershipDTO membershipDTO;

    private ApiCallWrapper<MembershipDTO, MembershipDTO> failMembershipWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> successMembershipWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> failProcessRemnantFeeWrapper;
    private ApiCallWrapper<MembershipDTO, MembershipDTO> successProcessRemnantFeeWrapper;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        when(report.getErrorJoiner()).thenReturn(STRING_JOINER);
        when(report.getSuccessJoiner()).thenReturn(STRING_JOINER);
        failMembershipWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP, ApiCall.Result.FAIL)
                .request(membershipDTO)
                .response(membershipDTO)
                .message(FAIL_MSG)
                .build();
        successMembershipWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP, ApiCall.Result.SUCCESS)
                .request(membershipDTO)
                .response(membershipDTO)
                .message(SUCCESS_MSG)
                .build();
        failProcessRemnantFeeWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.PROCESS_REMNANT_FEE, ApiCall.Result.FAIL)
                .request(membershipDTO)
                .response(membershipDTO)
                .message(FAIL_MSG)
                .build();
        successProcessRemnantFeeWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.PROCESS_REMNANT_FEE, ApiCall.Result.SUCCESS)
                .request(membershipDTO)
                .response(membershipDTO)
                .message(SUCCESS_MSG)
                .build();
    }

    @Test
    public void testFeedReport() {
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(failProcessRemnantFeeWrapper, report);
        reportService.feedReport(successProcessRemnantFeeWrapper, report);
        then(report).should(times(3)).getErrorJoiner();
        then(report).should(times(2)).getSuccessJoiner();
    }

    @Test
    public void testGenerateReport() {
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(failProcessRemnantFeeWrapper, report);
        reportService.feedReport(successProcessRemnantFeeWrapper, report);
        reportService.generateReportLog(report);
        then(report).should(times(4)).getErrorJoiner();
        then(report).should(times(3)).getSuccessJoiner();
    }

    @Test
    public void testGenerateReportOnlySuccesses() {
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(successMembershipWrapper, report);
        reportService.feedReport(successProcessRemnantFeeWrapper, report);
        reportService.generateReportLog(report);
        then(report).should().getErrorJoiner();
        then(report).should(times(4)).getSuccessJoiner();
    }

    @Test
    public void testGenerateReportOnlyFails() {
        reportService.feedReport(failMembershipWrapper, report);
        reportService.feedReport(failProcessRemnantFeeWrapper, report);
        reportService.generateReportLog(report);
        then(report).should(times(3)).getErrorJoiner();
        then(report).should().getSuccessJoiner();
    }


}
