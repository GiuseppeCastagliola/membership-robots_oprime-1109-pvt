package com.odigeo.membership.robots.configuration;

import bean.test.BeanTest;

public class BookingApiConfigurationTest extends BeanTest<BookingApiConfiguration> {

    private static final String PWD = "password";
    private static final String USER = "user";

    @Override
    protected BookingApiConfiguration getBean() {
        BookingApiConfiguration bookingApiConfiguration = new BookingApiConfiguration();
        bookingApiConfiguration.setPassword(PWD);
        bookingApiConfiguration.setUser(USER);
        return bookingApiConfiguration;
    }
}