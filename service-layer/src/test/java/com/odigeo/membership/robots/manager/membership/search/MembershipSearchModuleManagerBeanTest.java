package com.odigeo.membership.robots.manager.membership.search;

import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.mapper.request.MembershipSearchRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipSearchModuleManagerBeanTest {
    public static final String INTERNAL_ERROR_MSG = "FAILED";
    public static final MembershipInternalServerErrorException INTERNAL_SERVER_ERROR_EXCEPTION = new MembershipInternalServerErrorException(INTERNAL_ERROR_MSG);
    @Mock
    private MembershipSearchApi membershipSearchApi;

    private MembershipSearchModuleManager membershipSearchModuleManager;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipSearchModuleManager = new MembershipSearchModuleManagerBean(membershipSearchApi, Mappers.getMapper(MembershipSearchRequestMapper.class), Mappers.getMapper(MembershipResponseMapper.class));
    }

    @Test
    public void testSearchMemberships() {
        given(membershipSearchApi.searchMemberships(any(MembershipSearchRequest.class))).willReturn(Collections.emptyList());
        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships = membershipSearchModuleManager.searchMemberships(SearchMembershipsDTO.builder().build());
        assertTrue(searchMemberships.isSuccessful());
        assertNull(searchMemberships.getException());
        assertNotNull(searchMemberships.getResponse());
    }

    @Test
    public void testSearchMembershipsWithException() {
        given(membershipSearchApi.searchMemberships(any(MembershipSearchRequest.class))).willThrow(INTERNAL_SERVER_ERROR_EXCEPTION);
        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships = membershipSearchModuleManager.searchMemberships(SearchMembershipsDTO.builder().build());
        assertFalse(searchMemberships.isSuccessful());
        assertEquals(searchMemberships.getException(), INTERNAL_SERVER_ERROR_EXCEPTION);
        assertEquals(searchMemberships.getMessage(), INTERNAL_ERROR_MSG);
        assertNull(searchMemberships.getResponse());
    }
}
