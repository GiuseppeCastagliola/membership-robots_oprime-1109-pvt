package com.odigeo.membership.robots.manager.bookingapi;

import com.odigeo.bookingapi.mock.v9.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v9.response.BuilderException;
import com.odigeo.bookingapi.mock.v9.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v9.response.FeeContainerBuilder;
import com.odigeo.bookingapi.v9.BadCredentialsException;
import com.odigeo.bookingapi.v9.BookingApiService;
import com.odigeo.bookingapi.v9.InvalidParametersException;
import com.odigeo.bookingapi.v9.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v9.responses.BookingDetail;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingBasicInfoBuilder;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.BookingApiConfiguration;
import com.odigeo.membership.robots.configuration.BookingSearchApiConfiguration;
import com.odigeo.membership.robots.dto.BookingDTO;
import com.odigeo.membership.robots.exceptions.bookingapi.BookingApiException;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import org.apache.commons.lang.StringUtils;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class BookingApiManagerBeanTest {
    private static final long BOOKING_ID = 123456789L;
    private static final long MEMBERSHIP_ID = 20202020L;
    private static final long USER_ID = 121212L;
    private static final BigDecimal TOTAL_AMOUNT = BigDecimal.TEN;
    private static final String ERROR_MESSAGE = "ERR_MSG";
    private static final String EXCEPTION_MESSAGE = "exception";
    private static final String CURRENCY_CODE = "EUR";
    @Mock
    private BookingApiService bookingApiService;
    @Mock
    private BookingSearchApiService bookingSearchApiService;
    @Mock
    private BookingApiConfiguration bookingApiConfiguration;
    @Mock
    private BookingSearchApiConfiguration bookingSearchApiConfiguration;

    private ApiCallWrapperBuilder<BookingDTO, BookingDTO> getBookingWrapperBuilder;
    private ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> searchBookingWrapperBuilder;
    private ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder;
    private SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse;
    private BookingDetail bookingDetail;

    private BookingApiManager bookingApiManager;

    @BeforeMethod
    public void setUp() throws BuilderException, com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        initMocks(this);
        bookingApiManager = new BookingApiManagerBean(bookingApiService, bookingSearchApiService, bookingApiConfiguration, bookingSearchApiConfiguration, Mappers.getMapper(BookingResponseMapper.class));
        setupSearchBookingRequestAndResponse();
        setupGetBookingRequestAndResponse();
        setupUpdateBookingRequestAndResponse();
    }

    @Test
    public void testGetBooking() {
        givenBookingApiSuccessfulResponse();
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        assertTrue(bookingDTOApiCallWrapper.isSuccessful());
        assertNull(bookingDTOApiCallWrapper.getException());
        assertEquals(bookingDTOApiCallWrapper.getResponse().getId(), BOOKING_ID);
    }

    @Test
    public void testGetBookingErrorMessage() {
        givenBookingApiReturnErrorMessage();
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        assertFalse(bookingDTOApiCallWrapper.isSuccessful());
        assertNull(bookingDTOApiCallWrapper.getException());
        assertEquals(bookingDTOApiCallWrapper.getMessage(), ERROR_MESSAGE);
    }

    @Test
    public void testGetBookingNullResponse() {
        givenBookingApiNullResponse();
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        assertFalse(bookingDTOApiCallWrapper.isSuccessful());
        assertNull(bookingDTOApiCallWrapper.getResponse());
        assertEquals(bookingDTOApiCallWrapper.getResult(), ApiCall.Result.FAIL);
    }

    @Test
    public void testBookingApiUndeclaredThrowableGetBooking() {
        givenBookingApiThrowsException(new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE)));
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        andWrapTheException(bookingDTOApiCallWrapper);
    }

    @Test
    public void testBookingApiInvalidParameterGetBooking() {
        givenBookingApiThrowsException(new InvalidParametersException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        andWrapTheException(bookingDTOApiCallWrapper);
    }

    @Test
    public void testBookingApiBadCredentialsGetBooking() {
        givenBookingApiThrowsException(new BadCredentialsException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        andWrapTheException(bookingDTOApiCallWrapper);
    }

    @Test
    public void testSearchBookings() {
        givenBookingApiSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getResponse().size(), 1);
        BookingDTO bookingDTO = searchBookingsApiCallWrapper.getResponse().get(0);
        assertEquals(bookingDTO.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(bookingDTO.getId(), BOOKING_ID);
    }

    @Test
    public void testSearchNonPrimeBookings() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        setupSearchNonPrimeBookingRequestAndResponse();
        givenBookingApiSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getMessage(), "1 non-Prime bookingSummaries found for membershipId " + MEMBERSHIP_ID);
        assertEquals(searchBookingsApiCallWrapper.getResult(), ApiCall.Result.SUCCESS);
        BookingDTO bookingDTO = searchBookingsApiCallWrapper.getResponse().get(0);
        assertEquals(bookingDTO.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(bookingDTO.getId(), BOOKING_ID);
    }

    @Test
    public void testSearchBookingsNoBookingsFound() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        setupSearchBookingRequestAndNoBookingsResponse();
        givenBookingApiSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getMessage(), "0 Prime bookingSummaries found for membershipId " + MEMBERSHIP_ID);
        assertEquals(searchBookingsApiCallWrapper.getResult(), ApiCall.Result.SUCCESS);
        assertEquals(searchBookingsApiCallWrapper.getResponse().size(), 0);
    }

    @Test
    public void testSearchBookingsNullResponse() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        setupSearchBookingRequestAndNullResponse();
        givenBookingApiSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getResult(), ApiCall.Result.FAIL);
        assertNull(searchBookingsApiCallWrapper.getResponse());
    }

    private void setupSearchNonPrimeBookingRequestAndResponse() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.FALSE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        List<BookingSummary> bookings = Collections.singletonList(new BookingSummaryBuilder()
                .bookingBasicInfo(new BookingBasicInfoBuilder().id(BOOKING_ID))
                .membershipId(MEMBERSHIP_ID).build(new Random()));
        searchBookingsPageResponse.setBookings(bookings);
    }

    private void setupSearchBookingRequestAndNullResponse() {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = null;
    }

    private void setupSearchBookingRequestAndNoBookingsResponse() {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
    }

    @Test
    public void testSearchBookingErrorMessage() {
        givenBookingApiReturnErrorMessage();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertFalse(searchBookingsApiCallWrapper.isSuccessful());
        assertNull(searchBookingsApiCallWrapper.getException());
        assertEquals(searchBookingsApiCallWrapper.getMessage(), ERROR_MESSAGE);
    }

    @Test
    public void testBookingApiUndeclaredThrowableSearchBooking() {
        givenBookingApiThrowsException(new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE)));
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        andWrapTheException(searchBookingsApiCallWrapper);
    }

    @Test
    public void testBookingApiInvalidParameterSearchBooking() {
        givenBookingApiThrowsException(new InvalidParametersException(EXCEPTION_MESSAGE));
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        andWrapTheException(searchBookingsApiCallWrapper);
    }

    @Test
    public void testBookingApiBadCredentialsSearchBooking() {
        givenBookingApiThrowsException(new BadCredentialsException(EXCEPTION_MESSAGE));
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        andWrapTheException(searchBookingsApiCallWrapper);
    }

    @Test
    public void testUpdateBooking() {
        givenBookingApiSuccessfulResponse();
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        assertTrue(updateBookingApiCallWrapper.isSuccessful());
        assertNull(updateBookingApiCallWrapper.getException());
        assertEquals(updateBookingApiCallWrapper.getResponse().getId(), BOOKING_ID);
    }

    @Test
    public void testUpdateBookingErrorMessage() {
        givenBookingApiReturnErrorMessage();
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        assertFalse(updateBookingApiCallWrapper.isSuccessful());
        assertNull(updateBookingApiCallWrapper.getException());
        assertEquals(updateBookingApiCallWrapper.getMessage(), ERROR_MESSAGE);
    }

    @Test
    public void testBookingApiUndeclaredThrowableUpdateBooking() {
        givenBookingApiThrowsException(new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE)));
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        andWrapTheException(updateBookingApiCallWrapper);
    }

    @Test
    public void testBookingApiInvalidParameterUpdateBooking() {
        givenBookingApiThrowsException(new InvalidParametersException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        andWrapTheException(updateBookingApiCallWrapper);
    }

    @Test
    public void testBookingApiBadCredentialsUpdateBooking() {
        givenBookingApiThrowsException(new BadCredentialsException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        andWrapTheException(updateBookingApiCallWrapper);
    }

    private void setupGetBookingRequestAndResponse() throws BuilderException {
        getBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING).request(BookingDTO.builder().id(BOOKING_ID).build());
        bookingDetail = new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new com.odigeo.bookingapi.mock.v9.response.BookingBasicInfoBuilder().id(BOOKING_ID))
                .userId(USER_ID)
                .currencyCode(CURRENCY_CODE)
                .allFeeContainers(Collections.singletonList(new FeeContainerBuilder().fees(Collections.singletonList(new FeeBuilder()))))
                .build(new Random());
    }

    private void setupSearchBookingRequestAndResponse() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        List<BookingSummary> bookings = Collections.singletonList(new BookingSummaryBuilder()
                .bookingBasicInfo(new BookingBasicInfoBuilder().id(BOOKING_ID))
                .membershipId(MEMBERSHIP_ID).build(new Random()));
        searchBookingsPageResponse.setBookings(bookings);
    }

    private void setupUpdateBookingRequestAndResponse() {
        updateBookingWrapperBuilder = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.UPDATE_BOOKING);
        updateBookingWrapperBuilder.request(new UpdateBookingRequestBuilder().build());
    }

    private void givenBookingApiSuccessfulResponse() {
        when(bookingSearchApiService.searchBookings(anyString(), anyString(), any(Locale.class), eq(searchBookingWrapperBuilder.getRequest())))
                .thenReturn(searchBookingsPageResponse);
        when(bookingApiService.getBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID)))
                .thenReturn(bookingDetail);
        when(bookingApiService.updateBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest())))
                .thenReturn(bookingDetail);
    }

    private void givenBookingApiThrowsException(Exception e) {
        when(bookingSearchApiService.searchBookings(anyString(), anyString(), any(Locale.class), any(SearchBookingsRequest.class))).thenThrow(e);
        when(bookingApiService.getBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID))).thenThrow(e);
        when(bookingApiService.updateBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()))).thenThrow(e);
    }

    private void givenBookingApiReturnErrorMessage() {
        when(bookingSearchApiService.searchBookings(anyString(), anyString(), any(Locale.class), eq(searchBookingWrapperBuilder.getRequest()))).thenReturn(searchBookingsPageResponse);
        when(bookingApiService.getBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID))).thenReturn(bookingDetail);
        when(bookingApiService.updateBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()))).thenReturn(bookingDetail);
        bookingDetail.setErrorMessage(ERROR_MESSAGE);
        searchBookingsPageResponse.setErrorMessage(ERROR_MESSAGE);
    }

    private void givenBookingApiNullResponse() {
        when(bookingApiService.getBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID))).thenReturn(null);
        when(bookingApiService.updateBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()))).thenReturn(null);
    }

    private void thenShouldInvokeApiGetBooking() {
        then(bookingApiService).should().getBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID));
    }

    private void thenShouldInvokeApiSearchBookings() {
        then(bookingSearchApiService).should().searchBookings(anyString(), anyString(), any(Locale.class), eq(searchBookingWrapperBuilder.getRequest()));
    }

    private void thenShouldInvokeApiUpdateBooking() {
        then(bookingApiService).should().updateBooking(anyString(), anyString(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()));
    }

    private void andWrapTheException(ApiCallWrapper apiCallWrapper) {
        assertFalse(apiCallWrapper.isSuccessful());
        assertEquals(apiCallWrapper.getException().getClass(), BookingApiException.class);
        assertFalse(StringUtils.isEmpty(apiCallWrapper.getMessage()));
    }

}