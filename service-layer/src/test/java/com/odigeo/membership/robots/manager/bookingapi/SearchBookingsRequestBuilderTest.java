package com.odigeo.membership.robots.manager.bookingapi;

import bean.test.BeanTest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import org.apache.commons.lang3.BooleanUtils;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchBookingsRequestBuilderTest extends BeanTest<SearchBookingsRequestBuilder> {

    private static final Long MEMBERSHIP_ID = 1L;

    @Override
    protected SearchBookingsRequestBuilder getBean() {
        return new SearchBookingsRequestBuilder()
                .bookingSubscriptionPrime(Boolean.TRUE)
                .membershipId(MEMBERSHIP_ID);
    }

    @Test
    public void testBuild() {
        SearchBookingsRequest searchBookingsRequest = getBean().build();
        assertTrue(BooleanUtils.toBoolean(searchBookingsRequest.getIsBookingSubscriptionPrime()));
        assertEquals(searchBookingsRequest.getMembershipId(), MEMBERSHIP_ID);
    }
}