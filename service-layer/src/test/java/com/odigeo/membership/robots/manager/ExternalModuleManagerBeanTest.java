package com.odigeo.membership.robots.manager;

import com.odigeo.bookingapi.v9.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationPeriod;
import com.odigeo.membership.robots.dto.BookingDTO;
import com.odigeo.membership.robots.dto.FeeContainerDTO;
import com.odigeo.membership.robots.dto.FeeDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.membership.MembershipFeeContainerTypes;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.manager.offer.MembershipOfferModuleManager;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ExternalModuleManagerBeanTest {
    private static final String WEBSITE = "ES";
    private static final long MEMBERSHIP_ID = 123456L;
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final MembershipDTO MEMBERSHIP_DTO = MembershipDTO.builder().id(MEMBERSHIP_ID).balance(BALANCE).website(WEBSITE).build();
    private static final long BOOKING_ID = 999L;

    private static final BookingDTO.Builder bookingCommonBuilder = BookingDTO.builder()
            .id(BOOKING_ID)
            .membershipId(MEMBERSHIP_ID);
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> CONSUME_BALANCE_SUCCESS = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
            .response(MEMBERSHIP_DTO).request(MEMBERSHIP_DTO).result(ApiCall.Result.SUCCESS).build();
    private static BookingDTO BOOKING_RESPONSE_DTO = bookingCommonBuilder
            .feeContainers(Collections.singletonList(FeeContainerDTO.builder()
                    .feeContainerType(MembershipFeeContainerTypes.MEMBERSHIP_RENEWAL.name())
                    .fees(Collections.singletonList(FeeDTO.builder().subCode(MembershipFeeSubCodes.AE11.name()).build()))
                    .build()))
            .build();
    private static BookingDTO BOOKING_RESPONSE_DTO_NO_FEE_CONTAINER_TYPE = bookingCommonBuilder
            .feeContainers(Collections.singletonList(FeeContainerDTO.builder()
                    .fees(Collections.singletonList(FeeDTO.builder().subCode(MembershipFeeSubCodes.AE10.name()).build()))
                    .build()))
            .build();
    private static BookingDTO BOOKING_RESPONSE_DTO_MULTIPLE_MEMBERSHIP_FEES = bookingCommonBuilder
            .feeContainers(Collections.singletonList(
                    FeeContainerDTO.builder()
                            .fees(Arrays.asList(
                                    FeeDTO.builder().subCode(MembershipFeeSubCodes.AE10.name()).build(),
                                    FeeDTO.builder().subCode(MembershipFeeSubCodes.AE11.name()).build()))
                            .build()))
            .build();
    private static BookingDTO BOOKING_RESPONSE_WRONG_FEES_DTO = bookingCommonBuilder
            .feeContainers(Collections.singletonList(FeeContainerDTO.builder()
                    .feeContainerType(MembershipFeeContainerTypes.MEMBERSHIP_RENEWAL.name())
                    .fees(Arrays.asList(FeeDTO.builder().subCode(MembershipFeeSubCodes.AE11.name()).build(), FeeDTO.builder().subCode(MembershipFeeSubCodes.AE10.name()).build()))
                    .build()))
            .build();
    private static BookingDTO BOOKING_RESPONSE_NO_FEES_DTO = bookingCommonBuilder
            .feeContainers(Collections.singletonList(FeeContainerDTO.builder()
                    .feeContainerType(MembershipFeeContainerTypes.MEMBERSHIP_RENEWAL.name())
                    .fees(Collections.emptyList())
                    .build()))
            .build();
    private static BookingDTO BOOKING_RESPONSE_MULTIPLE_CONTAINERS_DTO = bookingCommonBuilder
            .feeContainers(Arrays.asList(FeeContainerDTO.builder()
                    .feeContainerType(MembershipFeeContainerTypes.MEMBERSHIP_RENEWAL.name())
                    .fees(Collections.emptyList())
                    .build(), FeeContainerDTO.builder()
                    .feeContainerType(MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION.name())
                    .fees(Collections.emptyList())
                    .build()))
            .build();
    private static BookingDTO BOOKING_RESPONSE_NO_FEE_CONTAINER_DTO = bookingCommonBuilder
            .feeContainers(Collections.emptyList())
            .build();
    private static final ApiCallWrapper<BookingDTO, UpdateBookingRequest> UPDATE_BOOKING_SUCCESS = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(Endpoint.UPDATE_BOOKING)
            .response(BOOKING_RESPONSE_DTO)
            .result(ApiCall.Result.SUCCESS)
            .build();
    private static final ApiCallWrapper<BookingDTO, UpdateBookingRequest> UPDATE_BOOKING_SUCCESS_NO_FEE_CONTAINER_TYPE = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(Endpoint.UPDATE_BOOKING)
            .response(BOOKING_RESPONSE_DTO_NO_FEE_CONTAINER_TYPE)
            .result(ApiCall.Result.SUCCESS)
            .build();
    private static final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> GET_OFFER_RESPONSE_SUCCESS = new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(Endpoint.GET_OFFER)
            .result(ApiCall.Result.SUCCESS).response(MembershipOfferDTO.builder().price(BigDecimal.TEN).website(WEBSITE).build()).build();
    private static final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> GET_OFFER_RESPONSE_FAILED = new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(Endpoint.GET_OFFER)
            .result(ApiCall.Result.FAIL).exception(new Exception()).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> CREATE_PENDING_SUCCESS = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CREATE_PENDING_TO_COLLECT)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> SEARCH_BOOKING_SUCCESS = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(Endpoint.SEARCH_BOOKINGS)
            .response(Collections.singletonList(BOOKING_RESPONSE_DTO))
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_DTO)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> SEARCH_BOOKING_SUCCESS_NO_FEE_CONTAINER_TYPE = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(Endpoint.SEARCH_BOOKINGS)
            .response(Collections.singletonList(BOOKING_RESPONSE_DTO_NO_FEE_CONTAINER_TYPE))
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> SEARCH_BOOKING_SUCCESS_MULTIPLE_MEMBERSHIP_FEES = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(Endpoint.SEARCH_BOOKINGS)
            .response(Collections.singletonList(BOOKING_RESPONSE_DTO_MULTIPLE_MEMBERSHIP_FEES))
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_NO_FEE_CONTAINER_TYPE = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_DTO_NO_FEE_CONTAINER_TYPE)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_WRONG_FEES = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_WRONG_FEES_DTO)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_MULTIPLE_MEMBERSHIP_FEES = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_DTO_MULTIPLE_MEMBERSHIP_FEES)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_NO_FEES = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_NO_FEES_DTO)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_NO_CONTAINER = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_NO_FEE_CONTAINER_DTO)
            .result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_MULTIPLE_CONTAINER = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_RESPONSE_MULTIPLE_CONTAINERS_DTO)
            .result(ApiCall.Result.SUCCESS).build();
    private static final VerificationMode FIVE_TIMES = times(5);

    @Mock
    private MembershipOfferModuleManager membershipOfferModuleManager;
    @Mock
    private MembershipModuleManager membershipModuleManager;
    @Mock
    private MembershipSearchModuleManager membershipSearchModuleManager;
    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private ExpirationPeriod expirationPeriod;

    private ExternalModuleManagerBean externalModuleManagerBean;


    @BeforeMethod
    public void setup() {
        initMocks(this);
        when(expirationPeriod.getDaysInThePast()).thenReturn(1L);
        externalModuleManagerBean = new ExternalModuleManagerBean(expirationPeriod, membershipOfferModuleManager, membershipModuleManager, membershipSearchModuleManager,
                Mappers.getMapper(BookingRequestMapper.class), Mappers.getMapper(BookingResponseMapper.class), bookingApiManager);
    }

    @Test
    public void expireMembershipTest() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.EXPIRE_MEMBERSHIP)
                .result(ApiCall.Result.FAIL).build();
        given(membershipModuleManager.expireMembership(eq(MEMBERSHIP_DTO)))
                .willReturn(responseWrapper);
        assertEquals(externalModuleManagerBean.expireMembership(MEMBERSHIP_DTO), responseWrapper);
    }

    @Test
    public void retrieveRenewableMembershipsTest() {
        ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> responseWrapper = new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(Endpoint.SEARCH_MEMBERSHIPS)
                .build();
        given(membershipSearchModuleManager.searchMemberships(any(SearchMembershipsDTO.class))).willReturn(responseWrapper);
        assertEquals(externalModuleManagerBean.retrieveRenewableMemberships(), responseWrapper);
        then(membershipSearchModuleManager).should().searchMemberships(any(SearchMembershipsDTO.class));
    }

    @Test
    public void retrieveNotRenewableTest() {
        ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> deactivatedWrapper = new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(Endpoint.SEARCH_MEMBERSHIPS)
                .response(Collections.singletonList(MembershipDTO.builder().build())).build();
        given(membershipSearchModuleManager.searchMemberships(any(SearchMembershipsDTO.class))).willReturn(deactivatedWrapper);
        final ApiCallWrapper<List<MembershipDTO>, List<SearchMembershipsDTO>> retrieveNotRenewableMemberships = externalModuleManagerBean.retrieveNotRenewableMemberships();
        then(membershipSearchModuleManager).should(FIVE_TIMES).searchMemberships(any(SearchMembershipsDTO.class));
        assertEquals(retrieveNotRenewableMemberships.getResponse().size(), 5);
        assertEquals(retrieveNotRenewableMemberships.getRequest().size(), 5);
    }

    @Test
    public void createPendingToCollectMembershipTest() {
        given(membershipModuleManager.createPendingToCollect(eq(MEMBERSHIP_DTO), any(MembershipOfferDTO.class)))
                .willReturn(CREATE_PENDING_SUCCESS);
        given(membershipOfferModuleManager.getOffer(MEMBERSHIP_DTO))
                .willReturn(GET_OFFER_RESPONSE_SUCCESS);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingFirstCall = externalModuleManagerBean.createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO);
        then(membershipOfferModuleManager).should().getOffer(eq(MEMBERSHIP_DTO));
        assertEquals(pendingFirstCall, CREATE_PENDING_SUCCESS);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingSecondCall = externalModuleManagerBean.createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO);
        assertEquals(pendingSecondCall, pendingFirstCall);
        verifyNoMoreInteractions(membershipOfferModuleManager);
    }

    @Test
    public void createPendingToCollectGetOfferFailTest() {
        given(membershipOfferModuleManager.getOffer(MEMBERSHIP_DTO))
                .willReturn(GET_OFFER_RESPONSE_FAILED);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingWrapper = externalModuleManagerBean.createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO);
        assertFalse(createPendingWrapper.isSuccessful());
        assertEquals(createPendingWrapper.getException(), GET_OFFER_RESPONSE_FAILED.getException());
        verifyZeroInteractions(membershipModuleManager);
    }

    @Test
    public void testProcessRemnantFee() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS);
        given(bookingApiManager.updateBooking(eq(BOOKING_ID), any())).willReturn(UPDATE_BOOKING_SUCCESS);
        given(membershipModuleManager.consumeRemnantMembershipBalance(any())).willReturn(CONSUME_BALANCE_SUCCESS);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should().updateBooking(eq(BOOKING_ID), any());
        assertTrue(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeNoFeeContainerType() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS_NO_FEE_CONTAINER_TYPE);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_NO_FEE_CONTAINER_TYPE);
        given(bookingApiManager.updateBooking(eq(BOOKING_ID), any())).willReturn(UPDATE_BOOKING_SUCCESS_NO_FEE_CONTAINER_TYPE);
        given(membershipModuleManager.consumeRemnantMembershipBalance(any())).willReturn(CONSUME_BALANCE_SUCCESS);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should().updateBooking(eq(BOOKING_ID), any());
        assertTrue(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeSearchBookingsFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(Endpoint.SEARCH_BOOKINGS).result(ApiCall.Result.FAIL).exception(new Exception()).build());
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should(never()).getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeSearchBookingsMultipleResultFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(Endpoint.SEARCH_BOOKINGS).response(Arrays.asList(BOOKING_RESPONSE_DTO, BOOKING_RESPONSE_DTO)).result(ApiCall.Result.SUCCESS).build());
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should(never()).getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeGetBookingFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING).result(ApiCall.Result.FAIL).exception(new Exception()).build());
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeGetBookingNull() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(Endpoint.GET_BOOKING).result(ApiCall.Result.SUCCESS).build());
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeWrongFeesExceptionsFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_WRONG_FEES);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeMultipleMembershipFeesExceptionsFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS_MULTIPLE_MEMBERSHIP_FEES);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_MULTIPLE_MEMBERSHIP_FEES);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeNoFeesExceptionsFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_NO_FEES);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeNoContainerExceptionsFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_NO_CONTAINER);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeMultipleContainerExceptionsFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_MULTIPLE_CONTAINER);
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should(never()).updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }

    @Test
    public void testProcessRemnantFeeUpdateBookingFails() {
        given(bookingApiManager.searchBookings(any())).willReturn(SEARCH_BOOKING_SUCCESS);
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS);
        given(bookingApiManager.updateBooking(eq(BOOKING_ID), any())).willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(Endpoint.UPDATE_BOOKING).result(ApiCall.Result.FAIL).exception(new Exception()).build());
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeWrapper = externalModuleManagerBean.processRemnantFee(MEMBERSHIP_DTO);
        then(bookingApiManager).should().searchBookings(any());
        then(bookingApiManager).should().getBooking(any());
        then(bookingApiManager).should().updateBooking(eq(BOOKING_ID), any());
        assertFalse(processRemnantFeeWrapper.isSuccessful());
    }
}