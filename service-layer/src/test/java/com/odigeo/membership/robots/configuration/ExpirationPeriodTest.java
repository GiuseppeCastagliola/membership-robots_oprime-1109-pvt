package com.odigeo.membership.robots.configuration;

import bean.test.BeanTest;

public class ExpirationPeriodTest extends BeanTest<ExpirationPeriod> {

    @Override
    protected ExpirationPeriod getBean() {
        ExpirationPeriod expirationPeriod = new ExpirationPeriod();
        expirationPeriod.setDaysInThePast(0L);
        return expirationPeriod;
    }
}