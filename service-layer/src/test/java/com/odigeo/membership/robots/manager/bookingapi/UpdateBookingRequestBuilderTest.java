package com.odigeo.membership.robots.manager.bookingapi;

import bean.test.BeanTest;
import com.odigeo.bookingapi.v9.requests.FeeRequest;
import com.odigeo.bookingapi.v9.requests.UpdateBookingRequest;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertEquals;

public class UpdateBookingRequestBuilderTest extends BeanTest<UpdateBookingRequestBuilder> {

    private static final FeeRequest FEE_REQUEST = new FeeRequestBuilder().build();

    @Override
    protected UpdateBookingRequestBuilder getBean() {
        return new UpdateBookingRequestBuilder()
                .withFeeRequests(Collections.singletonList(FEE_REQUEST));
    }

    @Test
    public void testBuild() {
        UpdateBookingRequest updateBookingRequest = getBean().build();
        assertEquals(updateBookingRequest.getBookingFees().get(0), FEE_REQUEST);
    }
}
