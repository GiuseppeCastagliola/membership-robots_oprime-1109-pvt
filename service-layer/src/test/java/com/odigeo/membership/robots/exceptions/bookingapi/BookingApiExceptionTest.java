package com.odigeo.membership.robots.exceptions.bookingapi;

import bean.test.BeanTest;

public class BookingApiExceptionTest extends BeanTest<BookingApiException> {

    @Override
    protected BookingApiException getBean() {
        return new BookingApiException(new Exception());
    }
}