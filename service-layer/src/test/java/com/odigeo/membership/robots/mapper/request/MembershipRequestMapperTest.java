package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MembershipRequestMapperTest {
    private static final long TEST_ID = 1L;
    private static final String TEST_WEBSITE = "ES";
    private static final String NON_RECURRING = "NON_RECURRING";
    private static final String BUSINESS = "BUSINESS";
    private static final String POST_BOOKING = "POST_BOOKING";
    private static final long TEST_MEMBER_ACCOUNT_ID = 3L;
    private static final String EUR = "EUR";
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final String NEW_EXPIRATION_DATE = NOW.plusYears(1L).format(DateTimeFormatter.ISO_DATE);
    private static final String EXPIRE_OPERATION = "EXPIRE_MEMBERSHIP";
    private static final MembershipDTO TEST_MEMBERSHIP_DTO = MembershipDTO.builder().id(TEST_ID)
            .website(TEST_WEBSITE)
            .recurringId(NON_RECURRING)
            .membershipType(BUSINESS)
            .sourceType(POST_BOOKING)
            .memberAccountId(TEST_MEMBER_ACCOUNT_ID)
            .currencyCode(EUR)
            .expirationDate(NOW)
            .build();
    private static final MembershipOfferDTO MEMBERSHIP_OFFER_DTO = MembershipOfferDTO.builder()
            .currencyCode(EUR)
            .price(BigDecimal.TEN)
            .build();

    private static final MembershipRequestMapper MAPPER = Mappers.getMapper(MembershipRequestMapper.class);

    @Test
    public void dtoToExpireRequestTest() {
        final UpdateMembershipRequest updateMembershipRequest = MAPPER.dtoToExpireRequest(TEST_MEMBERSHIP_DTO);
        assertEquals(updateMembershipRequest.getMembershipId(), String.valueOf(TEST_MEMBERSHIP_DTO.getId()));
        assertEquals(updateMembershipRequest.getOperation(), EXPIRE_OPERATION);
        assertNull(updateMembershipRequest.getMembershipRenewal());
        assertNull(updateMembershipRequest.getMembershipStatus());
    }

    @Test
    public void dtoNullToExpireRequestTest() {
        final UpdateMembershipRequest updateMembershipRequest = MAPPER.dtoToExpireRequest(null);
        assertNull(updateMembershipRequest);
    }

    @Test
    public void dtoToCreatePendingToCollectRequestTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(TEST_MEMBERSHIP_DTO, MEMBERSHIP_OFFER_DTO, NEW_EXPIRATION_DATE).build();
        assertEquals(pendingToCollectRequest.getSubscriptionPrice(), MEMBERSHIP_OFFER_DTO.getPrice());
        assertEquals(pendingToCollectRequest.getExpirationDate(), NEW_EXPIRATION_DATE);
        assertEquals(pendingToCollectRequest.getMonthsToRenewal(), 12);
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullDtoTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(null, MEMBERSHIP_OFFER_DTO, NEW_EXPIRATION_DATE).build();
        assertNotNull(pendingToCollectRequest);
        assertEquals(pendingToCollectRequest.getSubscriptionPrice(), MEMBERSHIP_OFFER_DTO.getPrice());
        assertEquals(pendingToCollectRequest.getExpirationDate(), NEW_EXPIRATION_DATE);
        assertNull(pendingToCollectRequest.getMembershipType());
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullDateTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(TEST_MEMBERSHIP_DTO, MEMBERSHIP_OFFER_DTO, null).build();
        assertNotNull(pendingToCollectRequest);
        assertEquals(pendingToCollectRequest.getSubscriptionPrice(), MEMBERSHIP_OFFER_DTO.getPrice());
        assertNull(pendingToCollectRequest.getExpirationDate());
        assertNotNull(pendingToCollectRequest.getMembershipType());
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullPriceTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(TEST_MEMBERSHIP_DTO, null, NEW_EXPIRATION_DATE).build();
        assertNotNull(pendingToCollectRequest);
        assertNull(pendingToCollectRequest.getSubscriptionPrice());
        assertEquals(pendingToCollectRequest.getExpirationDate(), NEW_EXPIRATION_DATE);
        assertNotNull(pendingToCollectRequest.getMembershipType());
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullDtoAndDateTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(null, MEMBERSHIP_OFFER_DTO, null).build();
        assertNotNull(pendingToCollectRequest);
        assertNull(pendingToCollectRequest.getExpirationDate());
        assertEquals(pendingToCollectRequest.getSubscriptionPrice(), MEMBERSHIP_OFFER_DTO.getPrice());
        assertNull(pendingToCollectRequest.getMembershipType());
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullOfferAndDateTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(TEST_MEMBERSHIP_DTO, null, null).build();
        assertNotNull(pendingToCollectRequest);
        assertNotNull(pendingToCollectRequest.getMembershipType());
        assertNull(pendingToCollectRequest.getSubscriptionPrice());
        assertNull(pendingToCollectRequest.getExpirationDate());
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullDtoAndOfferTest() {
        final CreatePendingToCollectRequest pendingToCollectRequest = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(null, null, NEW_EXPIRATION_DATE).build();
        assertNotNull(pendingToCollectRequest);
        assertNull(pendingToCollectRequest.getMembershipType());
        assertNull(pendingToCollectRequest.getSubscriptionPrice());
        assertEquals(pendingToCollectRequest.getExpirationDate(), NEW_EXPIRATION_DATE);
    }

    @Test
    public void dtoToCreatePendingToCollectRequestNullTest() {
        final CreatePendingToCollectRequest.Builder pendingToCollectRequestBuilder = MAPPER
                .dtoToCreatePendingToCollectRequestBuilder(null, null, null);
        assertNull(pendingToCollectRequestBuilder);
    }

    @Test
    public void dtoToResetBalanceRequestNullTest() {
        final UpdateMembershipRequest updateMembershipRequest = MAPPER.dtoToResetBalanceRequest(null);
        assertNull(updateMembershipRequest);
    }

}