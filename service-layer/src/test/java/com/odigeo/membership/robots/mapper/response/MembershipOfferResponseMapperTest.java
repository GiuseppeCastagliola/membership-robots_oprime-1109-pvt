package com.odigeo.membership.robots.mapper.response;

import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MembershipOfferResponseMapperTest {

    private static final MembershipSubscriptionOffer MEMBERSHIP_SUBSCRIPTION_OFFER = MembershipSubscriptionOffer.builder()
            .setCurrencyCode("EUR").setDuration(1).setWebsite("ES").setPrice(new BigDecimal("54.99")).setOfferId("52865")
            .setProductId(213L).build();
    public static final MembershipOfferResponseMapper MEMBERSHIP_OFFER_RESPONSE_MAPPER = Mappers.getMapper(MembershipOfferResponseMapper.class);

    @Test
    public void subscriptionOfferToMembershipOfferDtoTest() {
        final MembershipOfferDTO membershipOfferDTO = MEMBERSHIP_OFFER_RESPONSE_MAPPER.subscriptionOfferToMembershipOfferDto(MEMBERSHIP_SUBSCRIPTION_OFFER);
        assertNotNull(membershipOfferDTO);
        assertEquals(membershipOfferDTO.getPrice(), MEMBERSHIP_SUBSCRIPTION_OFFER.getPrice());
        assertEquals(membershipOfferDTO.getWebsite(), MEMBERSHIP_SUBSCRIPTION_OFFER.getWebsite());
    }

    @Test
    public void subscriptionOfferNullToMembershipOfferDtoTest() {
        final MembershipOfferDTO membershipOfferDTO = MEMBERSHIP_OFFER_RESPONSE_MAPPER.subscriptionOfferToMembershipOfferDto(null);
        assertNull(membershipOfferDTO);
    }
}