package com.odigeo.membership.robots.bootstrap.bookingapi;

import com.odigeo.bookingapi.v9.BookingApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class BookingApiServiceModule extends AbstractRestUtilsModule {

    public BookingApiServiceModule(ServiceNotificator... serviceNotificators) {
        super(BookingApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpBookingApi(BookingApiService.class);
    }
}
