package com.odigeo.membership.robots.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationFilesManager;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.monitoring.dump.AutoDumpStateRegister;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.membership.client.enhanced.MembershipSearchApiModule;
import com.odigeo.membership.client.enhanced.MembershipServiceModule;
import com.odigeo.membership.offer.client.MembershipOfferServiceModule;
import com.odigeo.membership.robots.bootstrap.bookingapi.BookingApiServiceModule;
import com.odigeo.membership.robots.bootstrap.bookingapi.BookingSearchApiServiceModule;
import com.odigeo.membership.robots.configuration.MembershipRobotsModule;
import com.odigeo.robots.RobotFrameworkModule;
import com.odigeo.robots.RobotsBootstrap;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.net.URL;

import static com.edreams.configuration.ConfigurationEngine.getInstance;
import static com.edreams.configuration.ConfigurationEngine.init;

public class ContextListener implements ServletContextListener {

    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        final HealthCheckRegistry baseCheckerRegistry = new HealthCheckRegistry();
        final DumpStateRegistry dumpStateRegistry = new DumpStateRegistry();
        final AutoHealthCheckRegister autoHealthCheckRegister = new AutoHealthCheckRegister(baseCheckerRegistry);
        final AutoDumpStateRegister autoDumpStateRegister = new AutoDumpStateRegister(dumpStateRegistry);
        servletContext.log("Bootstrapping .....");
        Logger logger = initLog4J(servletContext);
        initConfigurationEngine(autoHealthCheckRegister, autoDumpStateRegister);
        servletContext.setAttribute(DumpServlet.REGISTRY_KEY, dumpStateRegistry);
        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, baseCheckerRegistry);
        initMetrics();
        initRobotFramework();
        logger.info("Bootstrapping finished!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        getInstance(RobotsBootstrap.class).shutdownScheduler();
        event.getServletContext().log("context destroyed");
    }

    private static String getAppName() {
        try {
            return InitialContext.doLookup("java:app/AppName");
        } catch (NamingException e) {
            throw new IllegalStateException("cannot get AppName", e);
        }
    }

    private void initConfigurationEngine(ServiceNotificator... serviceNotificators) {
        init(new RobotFrameworkModule(),
                new MembershipRobotsModule(),
                new MembershipSearchApiModule.Builder().build(serviceNotificators),
                new MembershipServiceModule.Builder().build(serviceNotificators),
                new MembershipOfferServiceModule.Builder().build(serviceNotificators),
                new BookingApiServiceModule(serviceNotificators),
                new BookingSearchApiServiceModule(serviceNotificators));
    }

    private Logger initLog4J(ServletContext servletContext) {
        VirtualFile virtualFile = VFS.getChild(ConfigurationFilesManager.getConfigurationFileUrl(LOG4J_FILENAME, this.getClass()).getFile());
        URL fileRealURL;
        try {
            fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
            servletContext.log("Log4j has been initialized with config file " + fileRealURL.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");
        } catch (IOException e) {
            throw new IllegalStateException("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be load", e);
        }
        return Logger.getLogger(ContextListener.class);
    }

    private void initRobotFramework() {
        getInstance(RobotsBootstrap.class).initScheduler("membership-com");
    }

    private void initMetrics() {
        MetricsManager.getInstance().addMetricsReporter(getAppName());
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STARTED);
    }
}
